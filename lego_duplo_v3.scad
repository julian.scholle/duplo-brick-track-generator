/* [Generate object] */
// Generate: 1 Brick, 2 Plate, 3 BasePlate, 4 Calibration parts, 5 Track
generate_object = 5; //[1:Brick,2:Plate,3:Base plate,4:Calibration parts,5:Track]
// Object knobs in X direction
generate_knobs_x = 2; //[1:20]
// Object knobs in Y direction
generate_knobs_y = 4; //[1:20]
// Brick height Z direction
generate_height_z = 1; //[1:20]

generate_second_calibration_track_end = 1; //[1:Yes,0:No]

/* [Basic brick dimensions + print corrections] */
// Unit size (or pitch)
unit = 16;
// Diameter of knob on top of brick
knob_diameter = 9.4;
// Height of knob
knob_length = 4.6;
// Hollow knob on top?
geo_hollow_knop = 1; //[1:Yes,0:No]
// Height of recess underside of brick (should always > knob_length)
stud_height = 5;
// Size of knob hole underside of brick (interfaces with knob)
stud_diameter = 9.4;
// Diameter of tube underside of brick (3 for 4x2brick)
tube_diameter = 13.2;
// Height of standard brick
brick_height = 19.2;
// Thickness of outside wall
brick_wall_thickness = 1.6;
// Horizontal tolerance/play at one side of a wall
horizontal_play = 0.1;
// Height of plate (Duplo: 50%; Lego 33%)
plate_height = 9.6;
// Height of baseplate
baseplate_height = 1.6;

/* [Basic train track dimensions + print corrections] */
// Half sleeper diameter of connector pin (train track)
track_pin = 9.2;
// Diameter of connector hole in half sleeper
track_hole = 9.9;
// Witdh of the part connecting pin & half sleeper
track_pin_width = 6.4;
// Track width
track_full_width = 54;
// Distance betweens wheels
track_wheel_width = 31.8;
// Geo Rim Height
geo_rim_height = 4.0;

/* [Additional features] */
// Some ribs at bottom, so no support is needed
geo_use_ribs = 1; //[1:Yes,0:No]

// CUSTOM PARAMETERS ======================================================================
/* [Track] */
// Straight = 0 or Curved track (Standard curved track=30)
track_angle = 0; // [-90:5:90]
// Length of track (studs)
track_length_studs = 8; // [2:16]
track_length = track_length_studs * unit;
// Radius of track (in case of curved track)
// track_radius=260;// Radius of curve (standard track curve R = 260 mm)
// How many sleepers are needed?
nr_of_sleepers = 1; // [0:5]
// Since old tracks (before 2003) use single sleepers:
use_full_sleepers = 1; //[1:Yes,0:No]
/* [Ramp] */
// Add what height should the track end (bricks)?
ramp_height = 0; // [0:10]
// Want to extend end of track, so a new track could be placed on this
ramp_extend_support = 0; //[1:Yes,0:No]
/* [Geometry] */
// Rail offset from ground:
geo_use_offset = 0; //[1:Yes,0:No]
// Use big ribs on side of track for climbing
geo_use_bigribs = 0; //[1:Yes,0:No]
// Use small ribs:
geo_use_smallribs = 1; //[1:Yes,0:No]
small_ribs_height = 0.6;
// Increase rim thickness for more robust track:
geo_rim_thickness_increase = 0.6; // should be >=0.

//====================================================================================
// Generate top click interface:
module GenerateKnobs(unitsX = 2, unitsY = 4, unitsZ = 1, angle_deg = 0)
{
    // Adding knobs:
    translate([ 0, 0, unitsZ * brick_height ]) rotate(angle_deg, [ 0, -1, 0 ])
    {
        for (dy = [-(unitsY - .5) * unit / 2:unit:(unitsY - .5) * unit / 2])
        {
            for (dx = [-(unitsX - .5) * unit / 2:unit:(unitsX - .5) * unit / 2])
            {
                translate([ dx + 0.25 * unit, dy + 0.25 * unit, 0 ]) difference()
                {
                    translate([ 0, 0, -1 ])
                    {
                        cylinder(h = knob_length + 0.5, d = knob_diameter, center = false);
                        translate([ 0, 0, knob_length + 0.5 ])
                            cylinder(h = 0.5, d1 = knob_diameter, d2 = knob_diameter - 1, center = false);
                    }

                    if (geo_hollow_knop)
                    {
                        translate([ 0, 0, .5 ])
                        {
                            cylinder(h = knob_length, d = knob_diameter - 2 * brick_wall_thickness, center = false);
                        }
                    }
                }
            }
        } // End knobs.
    }
}

// Generate bottom click interface: height=stud_height+1.
module GenerateStuds(unitsX = 2, unitsY = 4)
{
    translate([ 0, 0, stud_height / 2 ]) union()
    {
        difference()
        {
            union()
            {
                difference()
                { // Add recess frame:
                    translate([ 0, 0, 0.5 ]) cube(
                        [ unitsX * unit - 2 * horizontal_play, unitsY * unit - 2 * horizontal_play, stud_height + 1 ],
                        true);
                    cube(
                        [
                            unitsX * unit - 2 * brick_wall_thickness, unitsY * unit - 2 * brick_wall_thickness, 2 *
                            stud_height
                        ],
                        true);
                } // Add ribs:
                for (dy = [-(unitsY - .5) * unit / 2:unit:(unitsY - 1.5) * unit / 2])
                {
                    translate([ 0, dy + 0.25 * unit, 0 ])
                        cube([ unitsX * unit - 4 * horizontal_play, brick_wall_thickness, stud_height ], true);
                }
                for (dx = [-(unitsX - .5) * unit / 2:unit:(unitsX - 1.5) * unit / 2])
                {
                    translate([ dx + 0.25 * unit, 0, 0 ])
                        cube([ brick_wall_thickness, unitsY * unit - 4 * horizontal_play, stud_height ], true);
                }

            } // Subtract inner section, getting short ribs
            cube([ unitsX * unit - (unit - stud_diameter), unitsY * unit - (unit - stud_diameter), 2 * stud_height ],
                 true);

        } // Add roof (1mm thick) & Tubes: & long ribs:
        difference()
        {
            union()
            {
                translate([ 0, 0, stud_height / 2 + .5 ])
                {
                    cube([ unitsX * unit - 2 * horizontal_play, unitsY * unit - 2 * horizontal_play, 1 ], true);
                }
                if ((unitsX > 1) && (unitsY > 1))
                {
                    if (geo_use_ribs)
                    { // add optioanl long ribs:
                        for (dy = [-(unitsY - 2) / 2 * unit:unit:(unitsY - 2) / 2 * unit])
                        {
                            translate([ 0, dy, 0.25 ])
                                cube([ unitsX * unit - 4 * horizontal_play, brick_wall_thickness, 5.5 ], true);
                        }
                        for (dx = [-(unitsX - 2) / 2 * unit:unit:(unitsX - 2) / 2 * unit])
                        {
                            translate([ dx, 0, 0.25 ])
                                cube([ brick_wall_thickness, unitsY * unit - 4 * horizontal_play, 5.5 ], true);
                        }
                    }
                    // Add tubes:
                    for (dy = [-(unitsY - 1.5) / 2 * unit:unit:(unitsY - 1.5) / 2 * unit])
                    {
                        for (dx = [-(unitsX - 1.5) / 2 * unit:unit:(unitsX - 1.5) / 2 * unit])
                        {
                            translate([ dx + 0.25 * unit, dy + 0.25 * unit, 0 ])
                                cylinder($fn = 16, h = stud_height, d = tube_diameter, center = true);
                        }
                    } // End adding Tubes.
                }
            } // Remove innerside of tubes:
            for (dy = [-(unitsY - 1.5) / 2 * unit:unit:(unitsY - 1.5) / 2 * unit])
            {
                for (dx = [-(unitsX - 1.5) / 2 * unit:unit:(unitsX - 1.5) / 2 * unit])
                {
                    translate([ dx + 0.25 * unit, dy + 0.25 * unit, 0 ]) cylinder(
                        $fn = 8, h = stud_height + .1, d = tube_diameter - 2 * brick_wall_thickness, center = true);
                }
            }
        } // End subtract
    }     // End union all
}

// Generate standard brick:
module GenerateBrick(unitsX = 2, unitsY = 4, unitsZ = 1, angle_deg = 0, hasKnobs = true, hasStuds = true)
{
    union()
    {
        rotate(90, [ 1, 0, 0 ])
        { // BASIC (ANGLED) BRICK BODY:
            H = (hasStuds) ? stud_height : 0;
            p0 = [ (unitsX * unit / 2 - horizontal_play), H ];
            p1 = [ p0[0], unitsZ * brick_height + tan(angle_deg) * (unitsX / 2 * unit - horizontal_play) ];
            p2 = [ -p0[0], unitsZ * brick_height - tan(angle_deg) * (unitsX / 2 * unit - horizontal_play) ];
            p3 = [ -p0[0], p0[1] ];
            linear_extrude(height = unitsY * unit - 2 * horizontal_play, center = true) polygon([ p0, p1, p2, p3 ]);
        }
        if (hasKnobs)
        {
            GenerateKnobs(unitsX, unitsY, unitsZ, angle_deg);
        }
        if (hasStuds)
        {
            GenerateStuds(unitsX, unitsY);
        }
    }
}

// Generate a plate:
module GeneratePlate(unitsX = 4, unitsY = 10)
{
    GenerateBrick(unitsX, unitsY, 0.5);
}

// Generate a baseplate:
module GenerateBasePlate(unitsX = 12, unitsY = 12, Zmm = baseplate_height)
{
    union()
    {
        linear_extrude(height = Zmm)
        {
            offset(r = unit / 4)
            {
                square([ (unitsX - 0.5) * unit - 2 * horizontal_play, (unitsY - 0.5) * unit - 2 * horizontal_play ],
                       center = true);
            }
        }
        GenerateKnobs(unitsX, unitsY, Zmm / brick_height, 0);
    }
}

// Generate track end sleeper:
module GenerateTrainTrackEnd()
{
    difference()
    {
        union()
        {
            translate([ unit / 2, 0, 0 ]) GenerateStuds(1, 4);
            translate([ unit / 2, 0, 7.5 ]) cube([ unit - 2 * horizontal_play, 27, 5 ], center = true);
            translate([ -unit / 4, -unit / 2, 5 ]) cube([ unit / 2 + 1, track_pin_width, 10 ], center = true);
            translate([ -unit / 2, -unit / 2, 5 ]) cylinder(d = track_pin, h = 10, center = true);
            translate([ unit / 4, unit / 2, 2.5 ])
                cube([ unit / 2 - 1, track_pin_width + 2 * brick_wall_thickness, 5 ], center = true);
            translate([ unit / 2, unit / 2, 2.5 ])
                cylinder(d = track_pin + 2 * brick_wall_thickness, h = 5, center = true);
            translate([ unit / 2, unit / 2, 2.5 ])
                cube([ brick_wall_thickness, 2 * unit - stud_diameter, 5 ], center = true);
        }
        translate([ unit / 4, unit / 2, 5 ]) cube([ unit / 2 + 1, track_pin_width + .45, 12 ], center = true);
        translate([ unit / 2, unit / 2, 5 ]) cylinder(d = track_hole, h = 12, center = true);
        translate([ -unit / 4, -unit / 2, 0 ]) rotate([ 90, 0, 0 ]) linear_extrude(height = 10, $fn = 3, center = true)
            polygon(points = [
                [ -(unit / 2 + 0.5), -0.5 ], [ -(unit / 2 + 0.5), 8 ], [ -(unit / 2 + 0.5) + track_pin, 4 ],
                [ unit / 4 + 0.1, 0 ], [ unit / 4 + 0.1, -0.5 ]
            ]);
    }
}

module CalibrationPieces()
{
    // Write down the numbers from above & print these pieces:

    // Testing studs:
    // stud_diameter=      //[mm] size of knob hole underside of brick (interfaces with knob)
    // stud_height=      //[mm] height of recess underside of brick
    // tube_diameter=      //[mm] diameter of tube underside of brick (3 for 4x2brick)
    // brick_wall_thickness= //[mm] Thickness of outside wall
    // horizontal_play=   //[mm] Horizontal tolerance/play at one side of a wall
    translate([ 0, 0, stud_height + 1 ]) rotate(180, [ 1, 0, 0 ]) GenerateStuds(generate_knobs_x, generate_knobs_y);

    // Testing the knobs:
    // knob_diameter=      //[mm] diameter of knob on top
    // knob_length=      //[mm] height of knob
    // baseplate_height= //[mm] Height of plate (according thing:1356494)
    translate([ generate_knobs_x * unit + 5, 0, 0 ]) GenerateBasePlate(generate_knobs_x, generate_knobs_y);

    translate([ 1.5 * generate_knobs_x * unit + 1.5 * unit, 0, 0 ]) GenerateTrainTrackEnd();
    if (generate_second_calibration_track_end)
        translate([ 1.5 * generate_knobs_x * unit + 4 * unit, 0, 0 ]) rotate([ 0, 0, 180 ]) GenerateTrainTrackEnd();
}

// TESTING: ==========================================================================
// render()
{
    if (generate_object == 1)
    {
        GenerateBrick(generate_knobs_x, generate_knobs_y, generate_height_z);
    }
    if (generate_object == 2)
    {
        GeneratePlate(generate_knobs_x, generate_knobs_y);
    }
    if (generate_object == 3)
    {
        GenerateBasePlate(generate_knobs_x, generate_knobs_y);
    }
    if (generate_object == 4)
    {
        CalibrationPieces();
    }
    if (generate_object == 5)
    {
        GenerateTrack();
    }
}

// Remarks:
// Don't make "bridges" to high:
// Close at the bottom, the top of train & wagon collide.
// At top, the carrier will tough the rim on the rail.

// CALCULATIONS =========================================================================
pi = 3.141592;
track_radius = track_length / sin(abs(track_angle));
lengthxy = (track_angle == 0) ? track_length : abs(track_angle / 360) * pi * 2 * track_radius;
ramp_tilt_angle = atan(ramp_height * brick_height / lengthxy);
geo_rail_zoffset = (geo_use_offset) ? 5 : 0;
sleeper_size = (use_full_sleepers) ? 2 : 1;

function DegToRad(degrees) = degrees * pi / 180;
function RadToDeg(radians) = radians * 180 / pi;

function GetCoefficients(x1, y1, x2, y2, k1, k2) = [
    (x2 * (x1 * (-x1 + x2) * (k2 * x1 + k1 * x2) - x2 * (-3 * x1 + x2) * y1) + pow(x1, 2) * (x1 - 3 * x2) * y2), // a0
    (k2 *x1 *(x1 - x2) * (x1 + 2 * x2) -
     x2 * (k1 * (-2 * pow(x1, 2) + x1 * x2 + pow(x2, 2)) + 6 * x1 * (y1 - y2))),                                  // a1
    (-k1 *(x1 - x2) * (x1 + 2 * x2) + k2 * (-2 * pow(x1, 2) + x1 * x2 + pow(x2, 2)) + 3 * (x1 + x2) * (y1 - y2)), // a2
    ((k1 + k2) * (x1 - x2) - 2 * y1 + 2 * y2)                                                                     // a3
] / pow((x1 - x2), 3);

// Parametic function, getting X position: 0<i<1
function GetX(i, offsetR = 0) = (track_angle == 0) ? i * lengthxy
                                                   : abs((track_radius + offsetR) * sin(i * (track_angle)));

// Parametic function, getting Y position: 0<i<1
function GetY(i, offsetR = 0) = (track_angle == 0)
                                    ? 0
                                    : sign(track_angle) *
                                          (track_radius - (track_radius + offsetR) * cos(i * -abs(track_angle)));

// Functiong getting Z position (or ramp angle): 0<i<1; depending on coefficients used
function GetZ(i, coef = c_heights) =
    coef[3] * pow(i * lengthxy, 3) + coef[2] * pow(i * lengthxy, 2) + coef[1] * i * lengthxy + coef[0];

// Functiong getting ramp angle: 0<i<1 [deg]
function GetTilt(i, coef = ramp_tilt_angle) = (coef[3] * pow(i * lengthxy, 3) + coef[2] * pow(i * lengthxy, 2) +
                                               coef[1] * i * lengthxy + coef[0]) *
                                              180 / pi;

// Calculate polynomal coefficients:
c_heights = GetCoefficients(0, 0, lengthxy, ramp_height *brick_height, 0, 0); //[mm]
c_angles = [ c_heights[1], 2 * c_heights[2], 3 * c_heights[3], 0 ];           //[rad]

// Generate path:
step_size = unit / lengthxy / 2;
path_pts = [for (i = [0:step_size:1 + 0.1 * step_size])[GetX(i), GetY(i), GetZ(i, c_heights)]];

// Calculated 3D curve length:
function d(n, step) = sqrt(pow(GetX(n + step) - GetX(n), 2) + pow(GetY(n + step) - GetY(n), 2) +
                           pow(GetZ(n + step, c_heights) - GetZ(n, c_heights), 2));
function add_up_to(n = 0, step = 0.05, sum = 0) = n >= 1 ? sum : add_up_to(n + step, step, sum + d(n, step));
length3d = add_up_to();

function GenerateSection3D(i, c, d = 1) = [
    [ 0, d *track_full_width / 2, geo_rail_zoffset ], [ 0, d *track_full_width / 2, GetZ(i, c) + 7.3 ],
    [ 0, d *track_wheel_width / 2, GetZ(i, c) + 7.3 ],
    [ 0, d *(track_wheel_width / 2 - 1.3), GetZ(i, c) + 7.3 + 2.7 + geo_rim_height ],
    [ 0, d *(track_wheel_width / 2 - 2.4 - geo_rim_thickness_increase), GetZ(i, c) + 7.3 + 2.7 + geo_rim_height ],
    [ 0, d *(track_wheel_width / 2 - 2.4 - geo_rim_thickness_increase), geo_rail_zoffset ],
    [ 0, d *(track_wheel_width / 2 - 2.4 - geo_rim_thickness_increase + brick_wall_thickness), geo_rail_zoffset ],
    [
        0, d *(track_wheel_width / 2 - 2.4 - geo_rim_thickness_increase + brick_wall_thickness),
        geo_use_offset ? geo_rail_zoffset : geo_rail_zoffset + 4
    ],
    [
        0, d *(track_full_width / 2 - brick_wall_thickness / 2),
        geo_use_offset ? geo_rail_zoffset : geo_rail_zoffset + 4
    ],
    [ 0, d *(track_full_width / 2 - brick_wall_thickness / 2), geo_rail_zoffset ],
    [ 0, d *track_full_width / 2, geo_rail_zoffset ]
];

function RotateSection3D(Section, angle = 0) = [for (j = [0:len(Section) - 1])
        rotate_p([ Section[j][0], Section[j][1], Section[j][2] ], angle, [ 0, 0, 1 ])];

function MoveSection3D(Section, OffsetX = 0, OffsetY = 0, OffsetZ = 0) =
    [for (j = [0:len(Section) - 1])[Section[j][0] + OffsetX, Section[j][1] + OffsetY, Section[j][2] + OffsetZ]];

function GetSectionTrackX(i, c, angle, dir = 1) = MoveSection3D(RotateSection3D(GenerateSection3D(i, c, dir), i *angle),
                                                                GetX(i), GetY(i), 0);

// BUILDING  TRACK =========================================================================
// GenerateTrack();

module GenerateRibs(pitch = 3, posy = 25.25, depth = 1.5, width = 3.25, height = 3.5)
{
    stepsize = pitch / length3d;
    for (i = [.5 * stepsize:stepsize:1])
    {
        translate([ GetX(i), GetY(i), GetZ(i, c_heights) ]) rotate(i * track_angle, [ 0, 0, 1 ])
            translate([ 0, posy, 7.3 ]) rotate(GetTilt(i, c_angles), [ 0, -1, 0 ])
                cube([ depth, width, height ], center = true);
    }
}

module GenerateRail(dir = -1)
{
    // Generate track cross section:
    allsections =
        dir == -1 ? [for (j = [0:(len(path_pts))]) GetSectionTrackX(j / len(path_pts), c_heights, track_angle, dir)]
                  : [for (j = [(len(path_pts)):-1:0]) GetSectionTrackX(j / len(path_pts), c_heights, track_angle, dir)];
    difference()
    {
        polysections(allsections);
        if (!geo_use_offset)
        {
            translate([ 0, 0, 0 ]) cube([ 2 * unit - .5, 4 * unit, 11 ], center = true);
            if (nr_of_sleepers > 0)
            {
                for (k = [1:nr_of_sleepers])
                {
                    i = k / (nr_of_sleepers + 1);
                    translate([ GetX(i), GetY(i), -1 ]) rotate((i)*track_angle,
                                                               [ 0, 0, 1 ]) translate([ 0, 0, stud_height / 2 ])
                        cube([ sleeper_size * (unit - horizontal_play), 4 * (unit - horizontal_play), stud_height + 3 ],
                             center = true);
                }
            }
        }
        translate([ GetX(1), GetY(1), -.5 ]) rotate(track_angle, [ 0, 0, 1 ]) if (ramp_extend_support)
        {
            GenerateBrick(unitsX = (2 * unit - 1) / unit, unitsY = 4, unitsZ = (GetZ(1, c_heights) - 1) / brick_height,
                          angle_deg = (GetTilt(1, c_angles)), hasKnobs = false, hasStuds = false);
        }
        else
        {
            GenerateBrick(unitsX = (2 * unit - 1) / unit, unitsY = 4, unitsZ = (GetZ(1, c_heights) + 6) / brick_height,
                          angle_deg = (GetTilt(1, c_angles)), hasKnobs = false, hasStuds = false);
        }
    }
}

module GenerateTrack()
{
    union()
    {
        // Generate start track brick:
        GenerateTrainTrackEnd();
        // Generate both track sides:
        // render_to_polygon()
        {
            GenerateRail(1);
            GenerateRail(-1);
        }
        // Generate end track brick:
        translate([ GetX(1), GetY(1), GetZ(0) ]) rotate(track_angle + 180, [ 0, 0, 1 ]) if (ramp_extend_support)
        {
            GenerateBrick(unitsX = 2, unitsY = 4, unitsZ = (GetZ(1 - step_size / 2)) / brick_height, angle_deg = 0,
                          hasKnobs = false, hasStuds = true);
            translate([ -unit / 2, 0, 0 ])
                GenerateKnobs(unitsX = 1, unitsY = 4, unitsZ = (GetZ(1 - step_size / 2)) / brick_height);
        }
        else
        {
            translate([ 0, 0, GetZ(1) ]) GenerateTrainTrackEnd();
        }
        // Generate sleepers:
        if (nr_of_sleepers > 0)
        {
            for (k = [1:nr_of_sleepers])
            {
                i = k / (nr_of_sleepers + 1);
                translate([ GetX(i), GetY(i), 0 ]) rotate((i)*track_angle, [ 0, 0, 1 ])
                    GenerateStuds(unitsX = sleeper_size, unitsY = 4);
            }
        }
        // Create ribs on side:
        if (geo_use_bigribs)
        {
            rib_width = (track_full_width - track_wheel_width) / 2 / 3.5;
            GenerateRibs(pitch = 3, posy = -(track_full_width - rib_width) / 2, depth = 1.5, width = rib_width,
                         height = 3.5);
            GenerateRibs(pitch = 3, posy = (track_full_width - rib_width) / 2, depth = 1.5, width = rib_width,
                         height = 3.5);
        }
        if (geo_use_smallribs)
        {
            rib_width = (track_full_width - track_wheel_width) / 2;
            if (track_angle >= 0)
            {
                GenerateRibs(pitch = 1, posy = (track_full_width - rib_width) / 2, depth = .5, width = rib_width,
                             height = small_ribs_height);
            }
            if (track_angle <= 0)
            {
                GenerateRibs(pitch = 1, posy = -(track_full_width - rib_width) / 2, depth = .5, width = rib_width,
                             height = small_ribs_height);
            }
        }
    }
}

function __to2d(p) = [ p[0], p[1] ];
function __to3d(p) = [ p[0], p[1], 0 ];
function __to_3_elems_ang_vect(a) = let(leng = len(a)) leng == 3 ? a : (leng == 2 ? [ a[0], a[1], 0 ] : [ a[0], 0, 0 ]);

function __to_ang_vect(a) = is_num(a) ? [ 0, 0, a ] : __to_3_elems_ang_vect(a);

function _q_rotate_p_3d(p, a, v) =
    let(half_a = a / 2, axis = v / norm(v), s = sin(half_a), x = s * axis[0], y = s * axis[1], z = s * axis[2],
        w = cos(half_a),

        x2 = x + x, y2 = y + y, z2 = z + z,

        xx = x * x2, yx = y * x2, yy = y * y2, zx = z * x2, zy = z * y2, zz = z * z2, wx = w * x2, wy = w * y2,
        wz = w * z2)[[ 1 - yy - zz, yx - wz, zx + wy ] * p, [ yx + wz, 1 - xx - zz, zy - wx ] * p,
                     [ zx - wy, zy + wx, 1 - xx - yy ] * p];

function _rotx(pt, a) = a == 0 ? pt
                               : let(cosa = cos(a),
                                     sina = sin(a))[pt[0], pt[1] * cosa - pt[2] * sina, pt[1] * sina + pt[2] * cosa];

function _roty(pt, a) = a == 0 ? pt
                               : let(cosa = cos(a),
                                     sina = sin(a))[pt[0] * cosa + pt[2] * sina, pt[1], -pt[0] * sina + pt[2] * cosa, ];

function _rotz(pt, a) = a == 0 ? pt
                               : let(cosa = cos(a),
                                     sina = sin(a))[pt[0] * cosa - pt[1] * sina, pt[0] * sina + pt[1] * cosa, pt[2]];

function _rotate_p_3d(point, a) = _rotz(_roty(_rotx(point, a[0]), a[1]), a[2]);

function _rotate_p(p, a) = let(angle = __to_ang_vect(a)) len(p) == 3 ? _rotate_p_3d(p, angle)
                                                                     : __to2d(_rotate_p_3d(__to3d(p), angle));

function _q_rotate_p(p, a, v) = len(p) == 3 ? _q_rotate_p_3d(p, a, v) : __to2d(_q_rotate_p_3d(__to3d(p), a, v));

function rotate_p(point, a, v) = is_undef(v) ? _rotate_p(point, a) : _q_rotate_p(point, a, v);

module polysections(sections, triangles = "SOLID")
{

    function side_indexes(sects, begin_idx = 0) =
        let(leng_sects = len(sects), leng_pts_sect = len(sects[0]),
            range_j = [begin_idx:leng_pts_sect:begin_idx + (leng_sects - 2) * leng_pts_sect],
            range_i = [0:leng_pts_sect - 1])
            concat([for (j = range_j) for (i = range_i)[j + i, j + (i + 1) % leng_pts_sect,
                                                        j + (i + 1) % leng_pts_sect + leng_pts_sect]],
                   [for (j = range_j) for (i = range_i)[j + i, j + (i + 1) % leng_pts_sect + leng_pts_sect,
                                                        j + i + leng_pts_sect]]);

    function search_at(f_sect, p, leng_pts_sect, i = 0) =
        i < leng_pts_sect ? (p == f_sect[i] ? i : search_at(f_sect, p, leng_pts_sect, i + 1)) : -1;

    function the_same_after_twisting(f_sect, l_sect, leng_pts_sect) =
        let(found_at_i = search_at(f_sect, l_sect[0], leng_pts_sect)) found_at_i <= 0
            ? false
            : l_sect == concat([for (i = [found_at_i:leng_pts_sect - 1]) f_sect[i]],
                               [for (i = [0:found_at_i - 1]) f_sect[i]]);

    function to_v_pts(sects) = [for (sect = sects) for (pt = sect) pt];

    module solid_sections(sects)
    {

        leng_sects = len(sects);
        leng_pts_sect = len(sects[0]);
        first_sect = sects[0];
        last_sect = sects[leng_sects - 1];

        v_pts = [for (sect = sects) for (pt = sect) pt];

        begin_end_the_same = first_sect == last_sect || the_same_after_twisting(first_sect, last_sect, leng_pts_sect);

        if (begin_end_the_same)
        {
            f_idxes = side_indexes(sects);

            polyhedron(v_pts, f_idxes, 10);
        }
        else
        {
            range_i = [0:leng_pts_sect - 1];
            first_idxes = [for (i = range_i) leng_pts_sect - 1 - i];
            last_idxes = [for (i = range_i) i + leng_pts_sect * (leng_sects - 1)];

            f_idxes = concat([first_idxes], side_indexes(sects), [last_idxes]);

            polyhedron(v_pts, f_idxes, 10);
        }
    }

    module hollow_sections(sects)
    {
        leng_sects = len(sects);
        leng_sect = len(sects[0]);
        half_leng_sect = leng_sect / 2;
        half_leng_v_pts = leng_sects * half_leng_sect;

        function strip_sects(begin_idx, end_idx) =
            [for (i = [0:leng_sects - 1])[for (j = [begin_idx:end_idx]) sects[i][j]]];

        function first_idxes() =
            [for (i = [0:half_leng_sect - 1])[i, i + half_leng_v_pts, (i + 1) % half_leng_sect + half_leng_v_pts,
                                              (i + 1) % half_leng_sect]];

        function last_idxes(begin_idx) =
            [for (i = [0:half_leng_sect -
                         1])[begin_idx + i, begin_idx + (i + 1) % half_leng_sect,
                             begin_idx + (i + 1) % half_leng_sect + half_leng_v_pts, begin_idx + i + half_leng_v_pts]];

        outer_sects = strip_sects(0, half_leng_sect - 1);
        inner_sects = strip_sects(half_leng_sect, leng_sect - 1);

        outer_v_pts = to_v_pts(outer_sects);
        inner_v_pts = to_v_pts(inner_sects);

        outer_idxes = side_indexes(outer_sects);
        inner_idxes = [for (idxes = side_indexes(inner_sects, half_leng_v_pts)) __reverse(idxes)];

        first_outer_sect = outer_sects[0];
        last_outer_sect = outer_sects[leng_sects - 1];
        first_inner_sect = inner_sects[0];
        last_inner_sect = inner_sects[leng_sects - 1];

        leng_pts_sect = len(first_outer_sect);

        begin_end_the_same = (first_outer_sect == last_outer_sect && first_inner_sect == last_inner_sect) ||
                             (the_same_after_twisting(first_outer_sect, last_outer_sect, leng_pts_sect) &&
                              the_same_after_twisting(first_inner_sect, last_inner_sect, leng_pts_sect));

        v_pts = concat(outer_v_pts, inner_v_pts);

        if (begin_end_the_same)
        {
            f_idxes = concat(outer_idxes, inner_idxes);

            polyhedron(v_pts, f_idxes, 10);
        }
        else
        {
            first_idxes = first_idxes();
            last_idxes = last_idxes(half_leng_v_pts - half_leng_sect);

            f_idxes = concat(first_idxes, outer_idxes, inner_idxes, last_idxes);

            polyhedron(v_pts, f_idxes, 10);
        }
    }

    module triangles_defined_sections()
    {
        module tri_sections(tri1, tri2)
        {
            hull() polyhedron(points = concat(tri1, tri2),
                              faces =
                                  [
                                      [ 0, 1, 2 ], [ 3, 5, 4 ], [ 1, 3, 4 ], [ 2, 1, 4 ], [ 2, 3, 0 ], [ 0, 3, 1 ],
                                      [ 2, 4, 5 ], [ 2, 5, 3 ]
                                  ],
                              10);
        }

        module two_sections(section1, section2)
        {
            for (idx = triangles)
            {
                tri_sections([ section1[idx[0]], section1[idx[1]], section1[idx[2]] ],
                             [ section2[idx[0]], section2[idx[1]], section2[idx[2]] ]);
            }
        }

        for (i = [0:len(sections) - 2])
        {
            two_sections(sections[i], sections[i + 1]);
        }
    }

    if (triangles == "SOLID")
    {
        solid_sections(sections);
    }
    else if (triangles == "HOLLOW")
    {
        hollow_sections(sections);
    }
    else
    {
        triangles_defined_sections();
    }
}